
if ! [ -x "$(command -v python3)" ]; then
  echo 'Error: python 3.x not found. Please install python>=3.7.'
  exit 1
fi

if ! python3 -c 'import sys; exit(1) if sys.version_info[0] < 3 else exit(0)'; then
  echo 'Error: python version 3.7 or greater required.'
  exit 1
fi

if ! python3 -c 'import sys; exit(1) if (sys.version_info[0] == 3 and sys.version_info[1] < 7) else exit(0)'; then
  echo 'Error: python version 3.7 or greater required.'
  exit 1
fi

if ! [ -x "$(command -v virtualenv)" ]; then
  echo 'Error: virtualenv not found. Please install virtualenv.'
  exit 1
fi

if ! [ -x "$(command -v pip)" ]; then
  echo 'Error: pip not found. Please install pip (should be generally installed with python).'
  exit 1
fi

if ! [ -a venv ]; then
  virtualenv venv
else
  echo 'Virtual env already exists. Skipping creation.'
fi

source env.sh
pip3 install -r requirements.txt
