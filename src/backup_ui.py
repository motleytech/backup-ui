
import os
import wx
import random
import math


from wx.lib.agw import ultimatelistctrl as ULC
import wx.lib.mixins.listctrl as listmix
from iconImages import folderIcon, movieIcon


PIPE_HEIGHT = 18
PIPE_WIDTH = 2000

ITEM_TITLE_FONT_SIZE = 11
ITEM_DETAILS_FONT_SIZE = 8
ITEM_LINE_GAP = 2


class ItemRenderer(object):

    DONE_BITMAP = None
    REMAINING_BITMAP = None

    def __init__(self, parent):

        self.progressValue = random.randint(1, 99)


    def DrawSubItem(self, dc, rect, line, highlighted, enabled):
        """Draw a custom progress bar using double buffering to prevent flicker"""

        canvas = wx.Bitmap(rect.width, rect.height)
        mdc = wx.MemoryDC()
        mdc.SelectObject(canvas)

        if highlighted:
            mdc.SetBackground(wx.Brush(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT)))
            mdc.SetTextForeground(wx.WHITE)
        else:
            mdc.SetBackground(wx.Brush(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW)))
        mdc.Clear()

        mdc.SetFont(wx.Font(ITEM_TITLE_FONT_SIZE, wx.FONTFAMILY_SCRIPT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False))

        if line == 0:
            text1 = "Apple Ads"
            text2 = "2.67 MB of 11.9 MB selected (22.53%) - 5 min 13 sec remaining"
            text3 = "Downloading from 1 of 1 peer - DL: 30.0 KB/s, UL: 0.0 KB/s"
            progress = 22.53
        else:
            text1 = "Apple TV Intro (HD).mov"
            text2 = "13.4 MB, uploaded 8.65 MB (Ratio: 0.64) - 1 hr 23 min remaining"
            text3 = "Seeding to 1 of 1 peer - UL: 12.0 KB/s"
            progress = 18.0

        ypos = ITEM_LINE_GAP + 5
        xtext, ytext = mdc.GetTextExtent(text1)
        mdc.DrawText(text1, 0, ypos)
        ypos += ytext + ITEM_LINE_GAP

        mdc.SetFont(wx.Font(ITEM_DETAILS_FONT_SIZE, wx.FONTFAMILY_SCRIPT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))

        xtext, ytext = mdc.GetTextExtent(text2)
        mdc.DrawText(text2, 0, ypos)
        ypos += ytext + ITEM_LINE_GAP

        self.DrawProgressBar(mdc, 0, ypos, rect.width, 20, progress)

        mdc.SetFont(wx.Font(ITEM_DETAILS_FONT_SIZE, wx.FONTFAMILY_SCRIPT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))
        ypos += 20 + ITEM_LINE_GAP

        mdc.DrawText(text3, 0, ypos)
        dc.Blit(rect.x+3, rect.y, rect.width-6, rect.height, mdc, 0, 0)


    def GetLineHeight(self):

        dc = wx.MemoryDC()
        dc.SelectObject(wx.Bitmap(1, 1))
        dc.SetFont(wx.Font(ITEM_TITLE_FONT_SIZE, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False))
        dummy, ytext1 = dc.GetTextExtent("Agw")
        dc.SetFont(wx.Font(ITEM_DETAILS_FONT_SIZE, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False))
        dummy, ytext2 = dc.GetTextExtent("Agw")

        dc.SelectObject(wx.NullBitmap)
        return ytext1 + 2*ytext2 + 40


    def GetSubItemWidth(self):

        return 250


    def DrawHorizontalPipe(self, dc, x, y, w, colour):
        """Draws a horizontal 3D-looking pipe."""

        for r in range(PIPE_HEIGHT):
            red = int(colour.Red() * math.sin((math.pi/PIPE_HEIGHT)*r))
            green = int(colour.Green() * math.sin((math.pi/PIPE_HEIGHT)*r))
            blue = int(colour.Blue() * math.sin((math.pi/PIPE_HEIGHT)*r))
            dc.SetPen(wx.Pen(wx.Colour(red, green, blue)))
            dc.DrawLine(x, y+r, x+w, y+r)


    def DrawProgressBar(self, dc, x, y, w, h, percent):
        """
        Draws a progress bar in the (x,y,w,h) box that represents a progress of
        'percent'. The progress bar is only horizontal and it's height is constant
        (PIPE_HEIGHT). The 'h' parameter is used to vertically center the progress
        bar in the allotted space.

        The drawing is speed-optimized. Two bitmaps are created the first time this
        function runs - one for the done (green) part of the progress bar and one for
        the remaining (white) part. During normal operation the function just cuts
        the necessary part of the two bitmaps and draws them.
        """

        # Create two pipes
        if self.DONE_BITMAP is None:
            self.DONE_BITMAP = wx.Bitmap(PIPE_WIDTH, PIPE_HEIGHT)
            mdc = wx.MemoryDC()
            mdc.SelectObject(self.DONE_BITMAP)
            self.DrawHorizontalPipe(mdc, 0, 0, PIPE_WIDTH, wx.GREEN)
            mdc.SelectObject(wx.NullBitmap)

            self.REMAINING_BITMAP = wx.Bitmap(PIPE_WIDTH, PIPE_HEIGHT)
            mdc = wx.MemoryDC()
            mdc.SelectObject(self.REMAINING_BITMAP)
            self.DrawHorizontalPipe(mdc, 0, 0, PIPE_WIDTH, wx.RED)
            self.DrawHorizontalPipe(mdc, 1, 0, PIPE_WIDTH-1, wx.WHITE)
            mdc.SelectObject(wx.NullBitmap)

        # Center the progress bar vertically in the box supplied
        y = y + (h - PIPE_HEIGHT)/2

        if percent == 0:
            middle = 0
        else:
            middle = (w * percent)/100

        if w < 1:
            return

        if middle == 0: # not started
            bitmap = self.REMAINING_BITMAP.GetSubBitmap((1, 0, w, PIPE_HEIGHT))
            dc.DrawBitmap(bitmap, x, y, False)
        elif middle == w: # completed
            bitmap = self.DONE_BITMAP.GetSubBitmap((0, 0, w, PIPE_HEIGHT))
            dc.DrawBitmap(bitmap, x, y, False)
        else: # in progress
            doneBitmap = self.DONE_BITMAP.GetSubBitmap((0, 0, middle, PIPE_HEIGHT))
            dc.DrawBitmap(doneBitmap, x, y, False)
            remainingBitmap = self.REMAINING_BITMAP.GetSubBitmap((0, 0, w - middle, PIPE_HEIGHT))
            dc.DrawBitmap(remainingBitmap, x + middle, y, False)

class TestUltimateListCtrl(ULC.UltimateListCtrl, listmix.ListCtrlAutoWidthMixin):

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=0, agwStyle=0):

        ULC.UltimateListCtrl.__init__(self, parent, id, pos, size, style, agwStyle)
        listmix.ListCtrlAutoWidthMixin.__init__(self)



class UltimateListCtrlPanel(wx.Panel):

    def __init__(self, parent, log):

        wx.Panel.__init__(self, parent, -1, style=wx.WANTS_CHARS)

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.log = log
        self.il = wx.ImageList(32, 32)

        self.il.Add(folderIcon.GetBitmap())
        self.il.Add(movieIcon.GetBitmap())

        self.list = TestUltimateListCtrl(self, -1,
                                         agwStyle=wx.LC_REPORT
                                         | wx.BORDER_SUNKEN
                                         #| wx.BORDER_NONE
                                         #| wx.LC_SORT_ASCENDING
                                         | wx.LC_NO_HEADER
                                         #| wx.LC_VRULES
                                         | wx.LC_HRULES
                                         #| wx.LC_SINGLE_SEL
                                         | ULC.ULC_HAS_VARIABLE_ROW_HEIGHT)

        self.list.SetImageList(self.il, wx.IMAGE_LIST_SMALL)
        sizer.Add(self.list, 1, wx.EXPAND)

        self.PopulateList()
        self.SetSizer(sizer)
        self.SetAutoLayout(True)

        self.Bind(wx.EVT_LIST_COL_BEGIN_DRAG, self.OnColBeginDrag, self.list)


    def PopulateList(self):

        self.list.Freeze()

        info = ULC.UltimateListItem()
        info.Mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_FORMAT
        info.Align = 0
        info.Text = ""

        self.list.InsertColumnInfo(0, info)

        info = ULC.UltimateListItem()
        info.Align = wx.LIST_FORMAT_LEFT
        info.Mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_FORMAT
        info.Image = []
        info.Text = "Some useful info here"

        self.list.InsertColumnInfo(1, info)

        for i in range(2):
            index = self.list.InsertImageStringItem(sys.maxsize, "", [i])
            self.list.SetStringItem(index, 1, "")
            klass = ItemRenderer(self)
            self.list.SetItemCustomRenderer(index, 1, klass)

        self.list.SetColumnWidth(0, 34)
        self.list.SetColumnWidth(1, 300)
        self.list.Thaw()
        self.list.Update()


    def OnColBeginDrag(self, event):

        if event.GetColumn() == 0:
            event.Veto()
            return

        event.Skip()


#---------------------------------------------------------------------------

class TestFrame(wx.Frame):

    def __init__(self, parent, log):

        wx.Frame.__init__(self, parent, -1, "Cross platform backup program in Python", size=(800, 600))

        self.log = log
        # Create the CustomTreeCtrl, using a derived class defined below
        self.ulc = UltimateListCtrlPanel(self, self.log)

        menuBar = wx.MenuBar()

        menu1 = wx.Menu()
        menu1.Append(101, "&New backup task...", "Add a new backup task")
        menu1.AppendSeparator()
        menu1.Append(102, "&Close", "Close this frame")
        # Add menu to the menu bar
        menuBar.Append(menu1, "&File")

        self.SetMenuBar(menuBar)

        self.Bind(wx.EVT_MENU, self.CloseWindow, id=102)

        self.CenterOnScreen()
        self.Show()

    def CloseWindow(self, event):
        self.Close()


#---------------------------------------------------------------------------

if __name__ == '__main__':
    import sys
    app = wx.App(0)
    frame = TestFrame(None, sys.stdout)
    frame.Show(True)
    app.MainLoop()


