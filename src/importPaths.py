

def fixImportPathsForMac():
    import sys
    import os

    rootpath = (os.path.dirname(os.path.abspath(os.getcwd())))
    path1 = os.path.join(rootpath, 'venv', 'lib', 'python3.7', 'site-packages')
    path2 = os.path.join(rootpath, 'venv', 'lib', 'python37.zip')
    path3 = os.path.join(rootpath, 'venv', 'lib', 'python3.7')
    path4 = os.path.join(rootpath, 'venv', 'lib', 'python3.7', 'lib-dynload')

    sys.path.extend([path1, path2, path3, path4])
