from wx.lib.embeddedimage import PyEmbeddedImage


folderIcon = PyEmbeddedImage(
    b"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAANkE3LLaAgAABI5J"
    b"REFUeJztls+LHEUUxz+vqntmYgL+RCFoEPyFR6/r1Vs8ehRviwTBq4f8B+rBePCg+QM0gig5"
    b"qFFERFdFQSNiiCRqyK5r1nVNsjuZ6ep673nontmJ6Uk25iLig0dR1d31/bxvdVU3/EciALv/"
    b"yYPSNXjw4MHF/fv3v3pDSDOxsLDQqdMZbz5323vublVV+Xg89pSS55z9RmJpacn7/f69XXrF"
    b"FQOR+1NKsry8jLsTQiCEQIzxigwxEttrYaYV6Sz4Cq3OQTeX7996mrKMqBnuDg7mBg7guF/+"
    b"jAiEIDz4+Cv0ej2KopgHcW0ANWffY4e44/Y9WK6ZvibT+bYn9pl+nTO/fHmYvY88xWAwoCzL"
    b"HUF02uKAjzfZ2tgAd1wVN8VdcbWmNWfqhgi4M/r1BKOHK4qimC7JtSA6AQRwzbgmMMfdGogJ"
    b"gCluTWuquDuj9d8Y/r5CykrOeSoeY7x+AABM8VQ1jljjAGbQthMITMEh0PRTXaOqmBmq+g8B"
    b"TBmdOwNEXHMrbFMQNwM3UJ0CoAncUdUpwKz9VVV1SoXOUTfWv3qXvLmBWG4m1wo0IZoQrSBX"
    b"0zG0ImgCHFObVm+TXXSV6AQQN9Jvpzh//MOmiroRFB3PgFQNTK6QnBBLiBtqDcAsBECv1+t8"
    b"G7sdsExZCsPvjlGtnCBE2qpTI5arFmA7g7UOzAAAxBhZXV09lVIaXR9AcEpPXPz8DfTCOQJO"
    b"mAhaRcip6VtTfbCEW7P+ua3czDh8+PDygQMHXgT+2DmAZwpqBrt6hOEawy9ex4frBHFExwRN"
    b"iDVLEDQR8pgoxu7BXVwaDkkpUZYlR48eXVlcXHx2bW3tCLBTBxyxTEGmF2kgNleovj6Cnz9L"
    b"jEJoKw5WNaljAs5d9zwAP7zA5vmLpJQ4ffr0D8AHwJ+dhc5zQDxTkumX0kAMehSjdfT42/jy"
    b"N0RPRIxozTIESwSUfp15aN+jDD9/hosXLmJmNTCeJw7zTkJTSqnxguYFFKAowWv850/x9VOE"
    b"Ox+EPXcgIjgZ8Qyu+OpP3Kp7GW4NyTkHIAJ5xwC7b7k7Bq8pJOOlNHdImwTwAONzcGYNBrcS"
    b"9tyJD26B2AcCEgW9NGY0HKJmMs/luQBBfJDWz3BTkWHwdwDaL1UBBtR/wsYGYhEJfQh9XARJ"
    b"GbXmVGTOX9dcgNGFFRVqKHMjFtsaLgOgASgjKFBDyGMkjcEyQXehZqhd/RTsBGiQK7yoIUgD"
    b"MGvkBEBbCG0h64AEiDkilTdfSdMbAChzozxxoAtgkmE7RUDaE5FrrP8cAIdg0PNtByZLIGzb"
    b"P6k+zwAISBBCKeScOXt2ecjkx+m6HIjgPSAIUgoSZRtiFiC3WQARDENdqerMyy8d+vH9j44d"
    b"ae+8LgDbXP2WrbXjF+pYIP1eiP1BDGURRKKIBMEEUQFFrDK3UbY8rDRtVRqHl/LyuX7xyccn"
    b"D9V1/VHr084BPjtZLY7rd57fVdx8XyyMEJ1YjoIEIwR3EXd3TFoXXHEy1u4GCfTK/a9tPQEc"
    b"u5rwJObt0b1LS0tPdl1YWFj4YAfzfrMT8f/jXxF/AROfMsX0/mQPAAAAAElFTkSuQmCC")

movieIcon = PyEmbeddedImage(
    b"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1B"
    b"AACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAA"
    b"ABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjM2qefiJQAACIZJREFUWEetV2lMXNcVxkkt"
    b"V/HSJk6dxO6iFrWhqiJVrVSpUvujf6K2SX90caVaUZpfURupUqv+qJQ2ctrihTiJMeDBxoSt"
    b"gAGz7zsMszIzDDDDMAuzD8zA8GZlGTAwX8+5eCxFnaGW2jecucy775773fOd7R3Le3TZF+0F"
    b"zz73eevpM2fy9g/28vJwLC+P/v7jAk3RR1yZAchLp9N5u7u7+/GtZIV/7/jlV7/5VSnL6ty3"
    b"tFptgdfrRcDnh81mh81updEKq/VQFhcXhVgsFiELCwtCzGYz5ufnodfrYaHfPr83sLBoe7fR"
    b"KT2bbbdjM3yyLNeUQlXgcrnhdrtI6QIcjiUSB+x2+2Ox2WwEyvYYVAaYyWSCTjeNyQk5lhz2"
    b"tN/vjxoXzP/ssPkuXHZuPN7wKT2Ok5zICkClUhV4PB5MyRXo6+uFy+WC0+kUsrS09FiygWLL"
    b"GGYMGBkZoXEGkUgUHq9nzWhe/KDT5v1iZsPPGPAUydNZAeg02gKf14NEPI5gMMgKwIDcbrbK"
    b"oWUYVDZgbCWmYWxsDEqlAhsbG1hfD6fdbk/YaLJcrXKtZ6XjU0DUarWwAJ+YuSUzEp8++HwB"
    b"+H1eeFk8vkegCJyLrEOUMSC2kHXRiinFFJQqJXZ2U9hIbkCSorTGm5gxm651Wr0XjnRKjUYj"
    b"nJBGjI6OYnl5GcuBAAI0BgJ++APLBIoBMRg/vF4f3GQlL4F2kXWYGrVGDe20Dnv7e0ildghE"
    b"kkBE0m6nK2A0Lfy1xBU9nRNEBsD29jZisZigYYUkuEIjCQMik0KlM6OhS4GypgkU35+ArHkc"
    b"HUMa6GYOI6GtvRW3b8tw5eoV3LxZjPb2dpCTpt0uV9Bstf1hxO47mxUEA2AK2Mvn5uawuroq"
    b"JBQKIRQMYc5kRVG9BpfKLXi9Poafdu3gJ70P8bOuFH5Rt4q3y42QNY5iaHgIzc0tuFdRiTt3"
    b"7uBeZSUaGuoxODREIJyBeaPxYk4ATAGfYmpqCuFwGOH1dRrXiWM33ipW4WsfruL58iTO3d/B"
    b"+bYdvERyviOFF2pT+N4nEr5T5ERhRS9URKNGrYRiSoHx8TG0tbXhX/X1TFWaou2PWQFknHBv"
    b"bw9bW1sUShFIkTDxv4I/l0/h6cIoTsliyG8I4Y36acg6RlDfN4LCxgFcvG+Bb2MPdw1b+NJ7"
    b"NtxrV1MUeeH2eMmpXSDzC0pZ5FOTfzoSACcVg8GAaDRKEsGE2oSX/+HEMyVRnLsTxNUuLRxO"
    b"L4WZJKwzorLiYvUSCmd2UTa3jePX1/DjawpyUD9CqyGxqWnBjJHhYWFVpUqRHUDGCTnNGo1G"
    b"AWBtTcLlBh1OFYXwudII3qqdQYAcMkZzUkTCgMaJS3VefL06jNcrNHj5YwtOlsZw6m9WjKuN"
    b"kAjkOtHIeUKhUAidaq02NwB2wocPHwoK+OFAIIhLsll89sYaLtwKonp0gTaPIR6LQ25045e1"
    b"PuTXELAGI/QmC35XqcXJsgSOX5NQ0yVHhCzIkognsJ3i6IpDpdYcTQGbn/xBAPARgJ/fNuHk"
    b"TQn5twLo0doRT8RpLob3W+fwrQdxXKzTY4Eo4Qz692Y9nilex4mPEyh/ICegCcToPqfq/oF+"
    b"RClFk+7cADgKOO1yKMZp4TKF36WKeZwsJv4/9KNimADQaRKUYPTziyjvU8Dh9iGR2CRKovjt"
    b"XQNREMaJjyJoGlDTcwkk6dkAJbQ5StWcX4jqowFQTQcno0QiwVkM7zXN4LlSCWdvEh13zVgN"
    b"SyLNcpZjIDwmN5LQzHvwykd28pV1PH/VAc2sWdznusCUplIpAeBIC7APsPknJyfFwk1SoDAs"
    b"4tsyH168m8D3KyWU9NjhXg6TaQ9BRKNx6KwreLvOgfzbIZyThfGbMgU5XwSbm5tCmIKOjo5D"
    b"J8xFQSYKOGx8Pp9AvbnJBSWCwmYlzlTE8Y3aJH7dFEVhdwCNEx50KL24N+rFO80B/KB2DefL"
    b"QvhyuYQ3PrHDbHGJzVkPhx8Xuf9KAVuAFzFvTMP21g7JNpZXVvFmlRrn70v4SmMK321I4tWm"
    b"OF57kMAPG2MoqIviQl0SL1Zv4mx5nH6n8EGzTlDEelgnU/pEFCiVSkxMTBwCYCHutiiElpeD"
    b"uNWjwo/anMhvD+KF1i18oTWFlx4k8UqbD7+qN+K1u2qca9nC6ZY0fl+lF5sy90xBa2vrk1HA"
    b"PHHy4IUp2pjL6jZJ6lGVNFiWUDWux/vd0/hLpw5XeqbRrpqDg9Lu7KID77Zp8U7LNBqHdORH"
    b"m0IPRxQXtSeiQJIkyoBr2NnZEcJRkfmflfH/PDK3QghY5j7PCd8hYetl1vHGXM6fiAKOArlc"
    b"Ljb+fwnnlUwU5MwDmSjgE3AIZlDzKdgq3BswIJEhKUq4WrJzZe7ziXmOPZ7XZ6KJrclz7NhM"
    b"xZGJiKOAsxZv0N/fj+vXr4vwaWpqQlVVlZirra0VXU5NTQ1mZ2dF08E+wwWMrVddXS3eG4qL"
    b"i9HY2Aiq/+IAbIUjKchYgF5QqLNVCgBFRUXo7e1FJXU1DIDvdXZ2Cm67u7vR09ODemo0dDqd"
    b"aMk5jWcAlJSUoKuri9q5FdG08vNsodwWoLacm4iDgwPs7x9gYGBAbHzjxg20tLQIxX19fUIR"
    b"V0wGNjQ0JDaXyWQCAFuCn+OwKy0tFc0tn35/fx8HpFNQkKscU5kkAB7qbum1zObAwOCA2KCh"
    b"oUH0+6yYW3WmgDdnCphf9gW2FHfT7D98nwGwBRg49wHc1Bqo1WML5CzHoiklAHPzc8TnrKj7"
    b"oh48yufsVHwS3pDfBXjk3yw8x89yO8dOyU532EW7D98vqIUfHx+n3uAICrT8XkDJBEgjnaZv"
    b"+vpfhZQd6qIPf3Mjo8nVkBj0MwV6nR6T8kkMDw6Sww0Izo8Sdspc85k51jNI+kbHRqGbniYq"
    b"Zj7VD/wb8xLWxx63hcwAAAAASUVORK5CYII=")

