echo 'Sourcing platform specific venv activation script'

if [[ "$OSTYPE" == "linux-gnu" ]]; then
        echo "Linux platform detected... sourced."
        source venv/bin/activate
elif [[ "$OSTYPE" == "darwin"* ]]; then
        echo "Mac platform detected... sourced."
        source venv/bin/activate
elif [[ "$OSTYPE" == "cygwin" ]]; then
        # cygwin
        echo "Cygwin platform detected... sourced."
        source venv/Scripts/activate
elif [[ "$OSTYPE" == "msys" ]]; then
        echo "Windows platform detected... sourced."
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
        source venv/Scripts/activate
elif [[ "$OSTYPE" == "win32" ]]; then
        # I'm not sure this can happen.
        echo "Windows platform detected... sourced."
        source venv/Scripts/activate
elif [[ "$OSTYPE" == "freebsd"* ]]; then
        echo "FreeBSD platform detected... sourced."
        source venv/bin/activate
fi
